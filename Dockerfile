FROM python:3.7.4-alpine
WORKDIR /opt/textbase
COPY textbase.py .
COPY convert_xlsx_to_dmp.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD ["/usr/local/bin/python", "/opt/textbase/convert_xlsx_to_dmp.py"]
