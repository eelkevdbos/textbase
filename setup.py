from distutils.core import setup

setup(
    name="TextBase",
    version="0.14",
    description="TextBase library to manipulate DBText style data files.",
    author="Etienne Posthumus",
    author_email="ep@epoz.org",
    url="https://github.com/epoz/textbase/",
    py_modules=["textbase"],
    extras_require={
        "xlsx": ["xlrd>=1.2.0", "XlsxWriter>=1.2.6", "progress>=1.5"]
    }
)
