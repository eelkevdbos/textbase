"""
Read the specified .xlsx files as edited by the project editors (like Eulalia from Naturalis, or Ernest from Brill)
And convert it into .dmp files that can be imported into Kosmeo

Assume that each Excel cell can have multiple values in the column, split with newlines.
The first row contains the *exact* fieldnames that wil be placed into the dump file.
Fieldnames can only contain a-z and underscores, and will be uppercased for legibility and Kosmeo compliance.

NOTE: the filename of the .xlsx file will become the TYPE of the .dmp file object!
"""

import xlrd
import os
from progress.bar import Bar


def process_row(fieldnames, arow):
    tmp = {}
    for idx, cell in enumerate(arow):
        if not cell.value:
            continue
        fieldname = fieldnames.get(idx)
        if not fieldname:
            continue
        val = str(cell.value)
        tmp[fieldname] = val.split("\n")
    return tmp


def dump(data):
    buf = []
    for obj in data:
        for k, v in obj.items():
            tmp = "\n; ".join(v)
            buf.append(f"{k} {tmp}")
        buf.append("$")
    return "\n".join(buf)


def main():
    INPUT_DIR = "/data"
    for dirpath, dirnames, filenames in os.walk(INPUT_DIR):
        for filename in filenames:
            if not filename.lower().endswith(".xlsx"):
                continue
            filepath = os.path.join(dirpath, filename)
            process_file(filepath)


def process_file(filepath):
    outfilepath = f"{filepath}.dmp"
    if os.path.exists(outfilepath):
        print(f"{outfilepath} exists, skipping")
        return

    sheet = xlrd.open_workbook(filepath).sheets()[0]

    # Get the fieldnames from row 0
    fieldnames = {}
    for idx, cell in enumerate(sheet.row(0)):
        if not cell.value:
            continue
        fieldnames[idx] = cell.value.upper()
    if "ID" not in fieldnames.values():
        print(f"{filepath} row 0 does not contain a field named ID", end=" :: ")
        print(" ".join(fieldnames.values()))
        return

    default_TYPE = os.path.split(filepath)[-1].replace(".xlsx", "").lower()
    data = []
    b = Bar(default_TYPE, max=sheet.nrows - 1)
    for row_i in range(1, sheet.nrows):
        b.next()
        obj = process_row(fieldnames, sheet.row(row_i))
        if "ID" not in obj:
            continue
        if "TYPE" not in obj:
            obj["TYPE"] = [default_TYPE]
        data.append(obj)
    b.finish()

    open(outfilepath, "w").write(dump(data))


if __name__ == "__main__":
    main()
